using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TenSecSushi {
    public class Emotion : MonoBehaviour {

        public static Emotion Instance { get; private set; }

        private void Awake() {
            Instance = this;
        }

        public void OnSuccess() {
            success.SetActive(true);
            failture.SetActive(false);
        }

        public void OnFailture() {
            success.SetActive(false);
            failture.SetActive(true);
        }

        public void Reset() {
            success.SetActive(false);
            failture.SetActive(false);
        }

        [SerializeField]
        GameObject success;
        [SerializeField]
        GameObject failture;
    }
}


