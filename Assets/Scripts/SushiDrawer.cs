using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TenSecSushi {
    public class SushiDrawer {

        public static SushiDrawer Instance { get { return instance; } }

        public void SetSlot(List<Data.Sushi> slot) {
            this.slot = slot;
        }

        public Data.Sushi Draw() {
            return slot[Random.Range(0, slot.Count)];
        }

        private SushiDrawer() { }

        private static SushiDrawer instance = new SushiDrawer();
        private List<Data.Sushi> slot;

    }
}
