using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeltMover : MonoBehaviour {

    public static readonly float BeltSpeed = 20.0f;

    private void FixedUpdate() {
        foreach(var belt in belts) {
            belt.Translate(Vector3.right * BeltSpeed * Time.deltaTime);

            if (belt.position.x > endPoint.position.x) {
                belt.position = startPoint.position;
            }
        }
    }

    [SerializeField]
    List<Transform> belts;
    [SerializeField]
    Transform startPoint;
    [SerializeField]
    Transform endPoint;
}
