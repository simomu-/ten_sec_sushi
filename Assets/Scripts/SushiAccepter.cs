using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TenSecSushi {
    public class SushiAccepter : MonoBehaviour {

        private static readonly int QueueSize = 3;

        public static SushiAccepter Instance { get; private set; }

        public int AcceptSushiCount { get; private set; }

        private void Awake() {
            Instance = this;
        }

        public void Initialize() {
            requestSushiQueue.Clear();
            for (int i = 0; i < QueueSize; ++i) {
                requestSushiQueue.Enqueue(SushiDrawer.Instance.Draw());
            }
            AcceptSushiCount = 0;

            UI.RequestSushiUIComponent.Instance.Initialize();
            
            Debug.LogFormat("First Request Sushi {0}", requestSushiQueue.Peek().Name);
        }

        public void ResetScore() {
            AcceptSushiCount = 0;
        }

        public bool Deliver(Data.Sushi sushi) {
            var requestSushi = requestSushiQueue.Peek();

            if (requestSushi.SushiType != sushi.SushiType) {
                return false;
            }

            requestSushiQueue.Dequeue();

            var newSushi = SushiDrawer.Instance.Draw();
            requestSushiQueue.Enqueue(newSushi);
            UI.RequestSushiUIComponent.Instance.Roll(newSushi);
            ResultDishes.Instance.AddResult();

            AcceptSushiCount++;

            Debug.LogFormat("Next Request Sushi {0}", requestSushiQueue.Peek().Name);
            return true;
        }

        public Data.Sushi[] GetRequestSlot() {
            return requestSushiQueue.ToArray();
        }

        Queue<Data.Sushi> requestSushiQueue = new Queue<Data.Sushi>();
    }
}
