using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TenSecSushi {
    public class ResultDishes : MonoBehaviour {

        public static ResultDishes Instance { get; private set; }
        static readonly float VerticalDistance = 2.8f;
        static readonly float HorizontalDistance = 0.3f;


        private void Awake() {
            Instance = this;
        }

        public void Initialize() {
            int childCount = transform.childCount;
            for(int i = 0; i < childCount; ++i) {
                Destroy(transform.GetChild(i).gameObject);
            }
        }

        public void AddResult() {
            var obj = Instantiate<GameObject>(dishPrefab);
            obj.SetActive(true);
            obj.transform.SetParent(transform, true);

            int count = transform.childCount;

            Vector3 position = new Vector3(0, count % 30 * HorizontalDistance , (count / 30) * -1 * VerticalDistance);
            obj.transform.localPosition = position;
        }

        [SerializeField]
        GameObject dishPrefab;
    }
}

