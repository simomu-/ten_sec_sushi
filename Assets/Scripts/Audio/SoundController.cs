using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TenSecSushi {
    namespace Audio {
        [RequireComponent(typeof(AudioSource))]
        public class SoundController : MonoBehaviour {

            public static SoundController Instance { get; private set; }

            private void Awake() {
                Instance = this;
                audioSource = GetComponent<AudioSource>();
            }

            public void OnSuccess() {
                audioSource.PlayOneShot(successSound);
            }

            public void OnFailture() {
                audioSource.PlayOneShot(failtureSound);
            }

            AudioSource audioSource;

            [SerializeField]
            AudioClip successSound;
            [SerializeField]
            AudioClip failtureSound;
        }
    }
}


