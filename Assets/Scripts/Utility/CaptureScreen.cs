using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TenSecSushi {
    namespace Utility {
        public class CaptureScreen : MonoBehaviour {
            [SerializeField]
            string outputFileNmae = "Icon.png";
            private void Update() {
                if (Input.anyKeyDown) {
                    ScreenCapture.CaptureScreenshot(outputFileNmae);
                }
            }
        }
    }
}
