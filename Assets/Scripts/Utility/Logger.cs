using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace TenSecSushi {
    namespace Utility {
        public class Logger : MonoBehaviour {

            private void Awake() {
                if (!Debug.isDebugBuild) {
                    gameObject.SetActive(false);
                    return;
                }
                Application.logMessageReceived += HandleLog;
            }

            private void OnDestroy() {
                Application.logMessageReceived -= HandleLog;
            }

            private void HandleLog(string logText, string stackTrace, LogType type) {

                string text = string.Empty;

                switch (type) {
                    case LogType.Log:
                        text = string.Format("{0}\n", logText);
                        break;
                    case LogType.Warning:
                        text = string.Format("<color=yellow>{0}\n{1}</color>\n", logText, stackTrace);
                        break;
                    default:
                        text = string.Format("<color=red>{0}\n{1}</color>\n", logText, stackTrace);
                        break;
                }

                message.text += text;
            }

            [SerializeField]
            Text message;
        }

    }
}
