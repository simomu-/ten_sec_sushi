using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TenSecSushi {
    namespace Utility {
        [RequireComponent(typeof(Camera))]
        public class CameraShake : MonoBehaviour {

            public static CameraShake Instance { get; private set; }

            [SerializeField] float shakeDecay = 0.002f;
            [SerializeField] float coefShakeIntensity = 0.3f;
            private Vector3 originPosition;
            private Quaternion originRotation;
            private float shakeIntensity;
            bool isShaking = false;

            void Awake() {
                Instance = this;
            }

            void Update() {
                if (shakeIntensity > 0) {
                    transform.position = originPosition + Random.insideUnitSphere * shakeIntensity;
                    transform.rotation = new Quaternion(
                                                     originRotation.x + Random.Range(-shakeIntensity, shakeIntensity) * 2f,
                                                     originRotation.y + Random.Range(-shakeIntensity, shakeIntensity) * 2f,
                                                     originRotation.z + Random.Range(-shakeIntensity, shakeIntensity) * 2f,
                                                     originRotation.w + Random.Range(-shakeIntensity, shakeIntensity) * 2f);
                    shakeIntensity -= shakeDecay;
                    isShaking = true;
                } else {
                    isShaking = false;
                }
            }

            public void Shake() {
                if (isShaking) {
                    return;
                }
                originPosition = transform.position;
                originRotation = transform.rotation;
                shakeIntensity = coefShakeIntensity;
            }
        }
    }
}


