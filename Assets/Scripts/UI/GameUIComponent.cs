using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

namespace TenSecSushi {
    namespace UI {
        public class GameUIComponent : MonoBehaviour {

            public static GameUIComponent Instance { get; private set; }

            public Layout.StageSelectScroll Scroller;

            private void Awake() {
                Instance = this;
            }

            public void SetSushiSlot(List<Data.Sushi> sushiDataList) {
                sushiSlotButtons.SetSushiSlot(sushiDataList);
            }

            public void SetCurrentTime(int currentTime) {
                currentTimeText.text = currentTime.ToString();
            }

            public void SetCurrentDishCount(int currentDishCount) {
                currentSuccessDishCountText.text = currentDishCount.ToString();
            }

            public void SetMessageText(string text, bool enable = true) {
                messageText.text = text;
                messageText.gameObject.SetActive(enable);
            }

            public void ShowResult(int count, int speed) {
                Scroller.MoveUp();

                resultCountText.text = string.Format("食らわせた寿司\n{0}個", count);
                resultSpeedText.text = string.Format("生産速度\n{0}個/h", speed);
            }

            public void Restart() {
                SceneManager.LoadScene("Main");
            }

            public void Tweet() {
                OnTweetButtonClick?.Invoke();
            }

            public void ShowRanking() {
                OnRankingButtonClick?.Invoke();
            }

            public UnityAction OnTweetButtonClick;
            public UnityAction OnRankingButtonClick;

            [SerializeField]
            SushiSlotButtons sushiSlotButtons;
            [SerializeField]
            Text currentTimeText;
            [SerializeField]
            Text currentSuccessDishCountText;
            [SerializeField]
            Text messageText;
            [SerializeField]
            Text resultCountText;
            [SerializeField]
            Text resultSpeedText;
        }
    }
}
