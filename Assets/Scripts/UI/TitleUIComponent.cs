using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace TenSecSushi {
    namespace UI {
        public class TitleUIComponent : MonoBehaviour {
            public void OnCick() {
                SceneManager.LoadScene(gameSceneName);
            }

            [SerializeField]
            string gameSceneName = "Main";
        }
    }
}


