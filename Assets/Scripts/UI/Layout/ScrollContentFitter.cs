using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace TenSecSushi {
    namespace UI {
        namespace Layout {
            public class ScrollContentFitter : MonoBehaviour {

                void Awake() {
                    Vector2 fixSize = transform.root.GetComponent<RectTransform>().sizeDelta;

                    layoutElement = GetComponent<LayoutElement>();
                    if (layoutElement != null) {
                        layoutElement.minWidth = fixSize.x;
                        layoutElement.minHeight = fixSize.y;
                    }

                    RectTransform rectTransform = GetComponent<RectTransform>();
                    rectTransform.sizeDelta = fixSize;

                }

                LayoutElement layoutElement;
                RectTransform rectTransform;
            }
        }
    }
}
