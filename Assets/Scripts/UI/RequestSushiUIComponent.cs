using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace TenSecSushi {
    namespace UI {
        public class RequestSushiUIComponent : MonoBehaviour {

            public static RequestSushiUIComponent Instance { get; private set; }

            static readonly float RollSpeed = 25.0f;

            public void Initialize() {
                var slot = SushiAccepter.Instance.GetRequestSlot();

                for(int i = 0; i < slot.Length; ++i) {
                    images[i].sprite = sprites[(int)slot[i].SushiType];
                    imageTransfromQueue.Enqueue(images[i]);
                }
            }

            private void Awake() {
                Instance = this;
            }

            private void Update() {
                //if (!GameState.GameStateMachine.Instance.IsControllable()) {
                //    return;
                //}

                var imageTransforms = imageTransfromQueue.ToArray();
                for (int i = 0; i < transforms.Count - 1; ++i) {
                    imageTransforms[i].transform.position = Vector3.Lerp(imageTransforms[i].transform.position, transforms[i].position, RollSpeed * Time.deltaTime);
                }
            }

            public void Roll(Data.Sushi newSushiData) {
                var image = imageTransfromQueue.Dequeue();
                image.transform.position = transforms[transforms.Count - 1].position;
                imageTransfromQueue.Enqueue(image);

                image.sprite = sprites[(int)newSushiData.SushiType];
            }

            Queue<Image> imageTransfromQueue = new Queue<Image>();

            [SerializeField]
            List<Transform> transforms;
            [SerializeField]
            List<Image> images;
            [SerializeField]
            List<Sprite> sprites;
        }
    }
}


