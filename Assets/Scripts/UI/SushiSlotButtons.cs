using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace TenSecSushi {
    namespace UI {
        public class SushiSlotButtons : MonoBehaviour {

            private void Start() {
                foreach (var trigger in buttons) {
                    buttonAnimators.Add(trigger.GetComponent<Animator>());
                }
            }

            public void SetSushiSlot(List<TenSecSushi.Data.Sushi> sushiList) {

                for(int i = 0; i < buttons.Count; ++i) {
                    var sushiData = sushiList[i];
                    var animator = buttonAnimators[i];
                    var eventEntry = new EventTrigger.Entry();
                    eventEntry.eventID = EventTriggerType.PointerDown;

                    eventEntry.callback.AddListener((eventData) => {

                        animator.SetTrigger("OnClick");

                        if (!GameState.GameStateMachine.Instance.IsControllable()) {
                            return;
                        }                      

                        if (!SushiAccepter.Instance.Deliver(sushiData)) {
                            Debug.Log("Invalid Sushi");
                            Utility.CameraShake.Instance.Shake();
                            GameState.GameStateMachine.Instance.InerruptControll();
                            Audio.SoundController.Instance.OnFailture();
                            Emotion.Instance.OnFailture();
                            return;
                        }
                        Debug.LogFormat("Create:{0}", sushiData.Name);
                        SushiFactory.Instance.CreateSushi(sushiData);
                        Audio.SoundController.Instance.OnSuccess();
                        Emotion.Instance.OnSuccess();
                    });
                    buttons[i].triggers.Add(eventEntry);
                    var imgae = buttons[i].transform.GetChild(0).GetComponent<Image>();
                    imgae.sprite = sprites[(int)sushiData.SushiType];
                }
            }

            List<Animator> buttonAnimators = new List<Animator>();

            [SerializeField]
            List<EventTrigger> buttons;
            [SerializeField]
            List<Sprite> sprites;
        }
    }
}
