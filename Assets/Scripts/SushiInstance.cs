using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TenSecSushi {
    public class SushiInstance : MonoBehaviour {

        public void SetSushiData(Data.Sushi sushiData) {
            this.sushiData = sushiData;
        }

        public void Update() {
            Move();

            if(transform.position.x >= 10) {
                Destroy(gameObject);
            }
        }

        public void Move() {
            transform.Translate(Vector3.right * moveSpeed * Time.deltaTime);
        }

        private Data.Sushi sushiData;
        readonly float moveSpeed = BeltMover.BeltSpeed;

    }
}


