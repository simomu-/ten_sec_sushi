using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace TenSecSushi {
    public class KeyboardInputHandler : MonoBehaviour {

        public static KeyboardInputHandler Instance { get; private set; }

        private void Awake() {
            Instance = this;
        }

        private void Start() {
            foreach(var trigger in eventTriggers) {
                buttinAnimators.Add(trigger.GetComponent<Animator>());
            }
        }

        private void Update() {
            if (Input.GetKeyDown(KeyCode.Alpha1) || Input.GetKeyDown(KeyCode.LeftArrow)){
                OnPointerDown(0);
            }

            if (Input.GetKeyDown(KeyCode.Alpha2) || Input.GetKeyDown(KeyCode.DownArrow)){
                OnPointerDown(1);
            }

            if (Input.GetKeyDown(KeyCode.Alpha3) || Input.GetKeyDown(KeyCode.RightArrow)){
                OnPointerDown(2);
            }
        }

        public void OnPointerDown(int slotIndex) {
            eventTriggers[slotIndex].OnPointerDown(new PointerEventData(eventSystem));
        }

        List<Animator> buttinAnimators = new List<Animator>();

        [SerializeField]
        List<EventTrigger> eventTriggers;
        [SerializeField]
        EventSystem eventSystem;
    }
}

