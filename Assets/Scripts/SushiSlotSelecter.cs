using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TenSecSushi {

    public static class SushiSlotSelecter{

        public static List<Data.Sushi> GenerateSushiSlot(int slotSize) {
            List<Data.Sushi> slot = new List<Data.Sushi>();
            var allData = new List<Data.Sushi>(allSushiDataList);

            for (int i = 0; i < slotSize; ++i) {
                //var sushi = allData[Random.Range(0, allData.Count)];
                var sushi = allData[0];
                slot.Add(sushi);
                allData.Remove(sushi);
            }
            return slot;
        }

        static List<Data.Sushi> allSushiDataList = new List<Data.Sushi>{
            new Data.Sushi(Data.SushiType.Tuna, "マグロ"),
            new Data.Sushi(Data.SushiType.Srhimp, "エビ"),
            new Data.Sushi(Data.SushiType.Egg, "卵"),
            new Data.Sushi(Data.SushiType.Salmon, "サーモン"),
            new Data.Sushi(Data.SushiType.SalmonRoe, "イクラ"),
            new Data.Sushi(Data.SushiType.SeaUrchin, "ウニ"),
        };
    }

}
