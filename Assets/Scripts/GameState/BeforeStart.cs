using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TenSecSushi {
    namespace GameState {
        public class BeforeStart : GameStateBase {

            static readonly float BeforeStartTime = 3.0f;

            public BeforeStart(MonoBehaviour monoBehaviour) : base(monoBehaviour) { }

            public override void Start() {
                Debug.Log("Start BeforeStart");

                monoBehaviour.StartCoroutine(StartCountDownCoroutine());
            }

            public override GameStateBase Update() {
                if (!isComplete) {
                    return this;
                }

                return new Playing(monoBehaviour);
            }

            public override void End() {
                UI.GameUIComponent.Instance.SetMessageText("Start", true);
            }

            public override void MarkStateComplete() {
                isComplete = true;
            }

            public override bool IsControllable() {
                return false;
            }

            public override void InterruptControll() {
            }

            private IEnumerator StartCountDownCoroutine() {

                float time = BeforeStartTime;
                UI.GameUIComponent.Instance.SetMessageText(((int)time).ToString());
                while (time > 0) {
                    UI.GameUIComponent.Instance.SetMessageText(((int)time).ToString());
                    Debug.LogFormat("Time:{0}", time);
                    yield return new WaitForSeconds(1.0f);
                    time -= 1.0f;
                }
                MarkStateComplete();
            }

            private bool isComplete = false;
        }
    }
}
