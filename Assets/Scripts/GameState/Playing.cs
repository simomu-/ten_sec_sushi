using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TenSecSushi {
    namespace GameState {
        public class Playing : GameStateBase {

            static readonly float GameTime = 10.0f;

            public Playing(MonoBehaviour monoBehaviour) : base(monoBehaviour) { }

            public override void Start() {
                Debug.Log("Playing");
                currentTime = GameTime;
                monoBehaviour.StartCoroutine(DisablwStartTextCoroutine());
            }

            public override GameStateBase Update() {

                UI.GameUIComponent.Instance.SetCurrentTime((int)currentTime + 1);

                if (isComplete) {
                    return new Finish(monoBehaviour);
                }

                currentTime -= Time.deltaTime;
                if (currentTime <= 0) {
                    currentTime = -1;
                    MarkStateComplete();
                }
                return this;
            }

            public override void End() {
            }

            public override void MarkStateComplete() {
                isComplete = true;
            }

            public override bool IsControllable() {
                return !isComplete && isControllable;
            }

            public override void InterruptControll() {
                monoBehaviour.StartCoroutine(InterruptControllCorotuine());
            }

            IEnumerator DisablwStartTextCoroutine() {
                yield return new WaitForSeconds(0.5f);
                UI.GameUIComponent.Instance.SetMessageText("", false);
            }

            IEnumerator InterruptControllCorotuine() {
                isControllable = false;
                yield return new WaitForSeconds(0.3f);
                isControllable = true;
            }

            private float currentTime = 0;
            bool isComplete = false;
            bool isControllable = true;
        }
    }
}
