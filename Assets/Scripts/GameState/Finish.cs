using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TenSecSushi {
    namespace GameState {
        public class Finish : GameStateBase {

            public Finish(MonoBehaviour monoBehaviour) : base(monoBehaviour) { }

            public override void Start() {
                Debug.Log("Finish");
                monoBehaviour.StartCoroutine(DisplayFinishTextCoroutine());
            }

            public override GameStateBase Update() {

                if (!isComplete) {
                    return this;
                }

                return new ShowResult(monoBehaviour);
            }

            public override void End() {
            }

            public override void MarkStateComplete() {
                isComplete = true;
            }

            public override bool IsControllable() {
                return false;
            }
            public override void InterruptControll() {
            }

            IEnumerator DisplayFinishTextCoroutine() {
                UI.GameUIComponent.Instance.SetMessageText("Finish", true);
                yield return new WaitForSeconds(1.0f);
                UI.GameUIComponent.Instance.SetMessageText("Finish", false);
                MarkStateComplete();
            }

            bool isComplete = false;
        }
    }
}
