using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TenSecSushi {
    namespace GameState {
        public class Tutorial : GameStateBase {

            public Tutorial(MonoBehaviour monoBehaviour, List<Data.Sushi> slot) : base(monoBehaviour) {
                this.slot = slot;
            }

            public override void Start() {
                Debug.Log("Start Tutorial");
                monoBehaviour.StartCoroutine(TutorialCoroutine());
            }

            public override GameStateBase Update() {

                if (isComplete) {
                    return new BeforeStart(monoBehaviour);
                }

                return this;
            }

            public override void End() {
                ResultDishes.Instance.Initialize();
                SushiAccepter.Instance.ResetScore();
                Emotion.Instance.Reset();
            }

            public override void MarkStateComplete() {
                isComplete = true;
            }

            public override bool IsControllable() {
                return false;
            }

            public override void InterruptControll() {
            }

            IEnumerator TutorialCoroutine() {

                var requestSushiSlot = SushiAccepter.Instance.GetRequestSlot();

                for (int i = 0; i < requestSushiSlot.Length - 1; ++i) {
                    yield return new WaitForSeconds(1.3f);
                    SushiFactory.Instance.CreateSushi(requestSushiSlot[i]);
                    KeyboardInputHandler.Instance.OnPointerDown(FindCorrectSlot(requestSushiSlot[i]));
                    SushiAccepter.Instance.Deliver(requestSushiSlot[i]);
                    Audio.SoundController.Instance.OnSuccess();
                    Emotion.Instance.OnSuccess();
                }

                yield return new WaitForSeconds(1.3f);
                SushiFactory.Instance.CreateSushi(slot[FindInvalidSlot(requestSushiSlot[requestSushiSlot.Length - 1])]);
                KeyboardInputHandler.Instance.OnPointerDown(FindInvalidSlot(requestSushiSlot[requestSushiSlot.Length - 1]));
                Utility.CameraShake.Instance.Shake();
                Audio.SoundController.Instance.OnFailture();
                Emotion.Instance.OnFailture();
                yield return new WaitForSeconds(1.0f);

                MarkStateComplete();
            }

            int FindCorrectSlot(Data.Sushi requestSushi) {
                int index = 0;
                foreach(var sushi in slot) {
                    if(requestSushi.SushiType == sushi.SushiType) {
                        break;
                    }
                    index++;
                }
                return index;
            }

            int FindInvalidSlot(Data.Sushi requestSushi) {
                int index = 0;
                foreach(var sushi in slot) {
                    if(requestSushi.SushiType != sushi.SushiType) {
                        break;
                    }
                    index++;
                }
                return index;
            }

            bool isComplete = false;
            List<Data.Sushi> slot;
        }
    }
}
