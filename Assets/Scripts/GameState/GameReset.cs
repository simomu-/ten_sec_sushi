using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TenSecSushi {
    namespace GameState {
        public class GameReset : GameStateBase {

            static readonly int SlotSize = 3;

            public GameReset(MonoBehaviour monoBehaviour) : base(monoBehaviour) { }

            public override void Start() {
                Debug.Log("Start GameReset");

                slot = SushiSlotSelecter.GenerateSushiSlot(SlotSize);
                UI.GameUIComponent.Instance.SetSushiSlot(slot);
                SushiDrawer.Instance.SetSlot(slot);
                SushiAccepter.Instance.Initialize();
                UI.GameUIComponent.Instance.SetMessageText("3", false);
            }

            public override GameStateBase Update() {
                return new Tutorial(monoBehaviour,slot);
            }

            public override void End() {
            }

            public override void MarkStateComplete() {
            }

            public override bool IsControllable() {
                return false;
            }

            public override void InterruptControll() {
            }

            private List<Data.Sushi> slot;
        }
    }
}
