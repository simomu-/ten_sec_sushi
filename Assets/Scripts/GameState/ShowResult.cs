using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TenSecSushi {
    namespace GameState {
        public class ShowResult : GameStateBase {

            public ShowResult(MonoBehaviour monoBehaviour) : base(monoBehaviour) { }

            public override void Start() {
                Debug.Log("ShowResult");

                int sushiCount = SushiAccepter.Instance.AcceptSushiCount;
                Debug.LogFormat("Accept Sushi Count:{0}", sushiCount);
                float sushiPerHour = (sushiCount / 10.0f) * 60 * 60;
                Debug.LogFormat("{0} sushi/h", sushiPerHour);

                UI.GameUIComponent.Instance.ShowResult(sushiCount, (int)sushiPerHour);
                UI.GameUIComponent.Instance.OnTweetButtonClick = () => {
                    string text = string.Format("食らわせた寿司の数:{0}個\n生産速度:{1}個/h\n", sushiCount, sushiPerHour);
                    naichilab.UnityRoomTweet.Tweet("ten_sec_sushi", text, "unity1week", "10secSUSHI");
                };

                UI.GameUIComponent.Instance.OnRankingButtonClick = () => {
                    naichilab.RankingLoader.Instance.SendScoreAndShowRanking(sushiCount);
                };
            }

            public override GameStateBase Update() {
                return this;
            }

            public override void End() {
            }

            public override void MarkStateComplete() {
            }

            public override bool IsControllable() {
                return false;
            }

            public override void InterruptControll() {
            }
        }
    }
}
