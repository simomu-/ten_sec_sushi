using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TenSecSushi {
    namespace GameState {
        public class GameStateMachine : MonoBehaviour {

            public static GameStateMachine Instance { get; private set; }

            GameStateBase gameState;

            private void Awake() {
                Instance = this;
            }

            private void Update() {

                if(gameState == null) {
                    gameState = new GameReset(this);
                    gameState.Start();
                }

                GameStateBase next = gameState.Update();
                if(next != gameState) {
                    gameState.End();
                    gameState = next;
                    gameState.Start();
                }
            }

            public bool IsControllable() {
                if(gameState == null) {
                    return false;
                }
                return gameState.IsControllable();
            }

            public void InerruptControll() {
                gameState.InterruptControll();
            }
        }
    }
}


