using UnityEngine;

namespace TenSecSushi {
namespace GameState {
    public abstract class GameStateBase {

        public GameStateBase(MonoBehaviour monoBehaviour) {
            this.monoBehaviour = monoBehaviour;
        }

        public abstract void Start();
        public abstract GameStateBase Update();
        public abstract void End();

        public abstract void MarkStateComplete();
        public abstract bool IsControllable();
        public abstract void InterruptControll();

        protected MonoBehaviour monoBehaviour;
    }
}
}
