
namespace TenSecSushi {
    namespace Data {
        public enum SushiType {
            Tuna = 0,
            Srhimp,
            Egg,
            Salmon,
            SalmonRoe,
            SeaUrchin
        }

        public class Sushi {
            public Sushi(SushiType sushiType, string name) {
                SushiType = sushiType;
                Name = name;
            }

            public SushiType SushiType { get; set; }
            public string Name { get; set; }
        }
    }
}
