using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TenSecSushi {
    public class SushiFactory : MonoBehaviour {

        public static SushiFactory Instance { get; private set; }

        private void Awake() {
            Instance = this;
        }

        public SushiInstance CreateSushi(Data.Sushi sushiData) {
            var sushi = Instantiate<SushiInstance>(sushiPrefabs[(int)sushiData.SushiType],createTransform.position, Quaternion.identity);
            sushi.SetSushiData(sushiData);
            return sushi;
        }

        [SerializeField]
        List<SushiInstance> sushiPrefabs;
        [SerializeField]
        Transform createTransform;
    }

}

